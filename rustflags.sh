#!/bin/bash
flags="-C link-arg=-s"

if [[ "${TARGET}" == *musl ]]; then 
    flags="${flags} -C target-feature=-crt-static"
fi

echo "${flags}"